
mkdir -p out/fastqc
mkdir -p log/cutadapt
mkdir -p out/cutadapt
mkdir -p res/genome/star_index
wget -nc -O res/genome/ecoli.fasta.gz ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/005/845/GCF_000005845.2_ASM584v2/GCF_000005845.2_ASM584v2_genomic.fna.gz
gunzip -f -k res/genome/ecoli.fasta.gz

echo "Running STAR index..."
STAR --runThreadN 4 --runMode genomeGenerate --genomeDir res/genome/star_index/ --genomeFastaFiles res/genome/ecoli.fasta --genomeSAindexNbases 9
echo

for sid in $(ls data/*.fastq.gz | cut -d "_" -f1 | sed 's:data/::' | sort | uniq)
do
    echo "Analyzing sample $sid"
    echo 
    bash scripts/analyse_sample2.sh $sid
    echo "Sample $sid analyzed"
done

echo "Running MultiQC"
mkdir -p out/multiqc
multiqc -f -o out/multiqc $(pwd)
